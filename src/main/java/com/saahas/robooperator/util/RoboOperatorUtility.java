package com.saahas.robooperator.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import com.saahas.robooperator.constant.RoboOperatorConstant;
import com.saahas.robooperator.vo.ComboBoxVO;
import com.saahas.robooperator.vo.TableDataVO;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class RoboOperatorUtility
{
	public void cloaseAppConfigStream(final InputStream inputStream)
	throws Throwable
	{
		if(inputStream == null)
		{
			return;
		}
		
		try
		{
			inputStream.close();
		}
		catch(Throwable eGenThrowable)
		{
			System.out.println(getProperty("ERR_CLOSING_STREAM") + " " + eGenThrowable);
		}
	}
	
	public InputStream createAppConfigStream() 
	throws Throwable
	{
		if (System.getProperty(RoboOperatorConstant.APP_CONFIG_FILE) == null)
		{
			return getClass().getResourceAsStream("/configuration/roboOperatorView.xml");
		}
		else
		{
			return new FileInputStream(new File(System.getProperty(RoboOperatorConstant.APP_CONFIG_FILE)));
		}
	}
	
	public ImageView getImageView(final String imagePath)
	throws Throwable
	{
		if(imagePath == null || imagePath.trim().equals(""))
		{
			return new ImageView();
		}
		
		System.out.println(getClass().getResourceAsStream(imagePath));

		if (System.getProperty(RoboOperatorConstant.APP_CONFIG_FILE) == null)
		{
			return new ImageView(new Image(getClass().getResourceAsStream(imagePath)));
		}
		else
		{
			return new ImageView(new Image("file:" + imagePath));
		}
	}

	public Image getImage(final String imagePath)
	throws Throwable
	{
		if(imagePath == null || imagePath.trim().equals(""))
		{
			return null;
		}

		if (System.getProperty(RoboOperatorConstant.APP_CONFIG_FILE) == null)
		{
			return new Image(getClass().getResourceAsStream(imagePath));
		}
		else
		{
			return new Image("file:" + imagePath);
		}
	}
	
	public boolean validateMenuListElement()
	throws Throwable
	{
		if(ObjectHolder.getInstance().getRoboOperatorView() == null)
		{
			throw new Exception(getProperty("INVALID_CONFIG_FILE_CONTENT"));
		}
		
		if(ObjectHolder.getInstance().getRoboOperatorView().getMenuListC() == null)
		{
			throw new Exception(getProperty("MENULIST_ELEMENT_MISSING"));
		}
		
		if(ObjectHolder.getInstance().getRoboOperatorView().getLanguageList().getLanguage() == null || ObjectHolder.getInstance().getRoboOperatorView().getLanguageList().getLanguage().size() == 0)
		{
			throw new Exception(getProperty("MENU_ELEMENT_MISSING"));
		}
		
		return true;
	}
	
	public boolean validateLanguageElement()
	throws Throwable
	{
		if(ObjectHolder.getInstance().getRoboOperatorView() == null)
		{
			throw new Exception(getProperty("INVALID_CONFIG_FILE_CONTENT"));
		}
		
		if(ObjectHolder.getInstance().getRoboOperatorView().getLanguageList() == null)
		{
			throw new Exception(getProperty("LANGUAGE_ELEMENT_MISSING"));
		}
		
		if(ObjectHolder.getInstance().getRoboOperatorView().getLanguageList().getLanguage() == null || ObjectHolder.getInstance().getRoboOperatorView().getLanguageList().getLanguage().size() == 0)
		{
			throw new Exception(getProperty("LANGUAGE_ELEMENT_MISSING"));
		}
		
		return true;
	}
	
	public Properties getResourceBundleAsProperty(final Locale locale) 
	throws Throwable
	{
		if(locale == null)
		{
			System.err.println("Invalid locale information. Please validate System settings.");
			return null;
		}
		
		final ResourceBundle resourceBundle = ResourceBundle.getBundle("com.saahas.robooperator.resourcebundle.roboOperator", locale);
		if(resourceBundle == null)
		{
			System.err.println("Invalid Resource bundle. Escalate to product team to create valide resource bundle");
			return null;
		}
		
		final Properties properties = new Properties();
		
		Enumeration<String> keys = resourceBundle.getKeys();
		while (keys.hasMoreElements())
		{
			String key = keys.nextElement();
			properties.put(key, resourceBundle.getString(key));
		}
		
		return properties;
	}
	
	public String getProperty(final String propertyKey)
	throws Throwable
	{
		final Properties properties = ObjectHolder.getInstance().getProperties();
		if(propertyKey == null)
		{
			throw new Exception(properties.getProperty("PROPERTY_KEY_INVALID"));
		}
		
		if(properties.getProperty(propertyKey.toUpperCase()) == null || properties.getProperty(propertyKey.toUpperCase()).trim().equals(""))
		{
			throw new Exception(properties.getProperty("PROPERTY_KEY_NOT_EXISTS") + propertyKey);
		}
		
		return properties.getProperty(propertyKey.toUpperCase());
	}
	
	public void resetTableView()
	{
		for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableView().getItems().size(); iCount++)
		{
			ObjectHolder.getInstance().getTableView().getItems().remove(iCount);
		}
		
		for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableView().getColumns().size(); iCount++)
		{
			ObjectHolder.getInstance().getTableView().getColumns().remove(iCount);
		}
		
		ObjectHolder.getInstance().setTableView(new TableView<TableDataVO>());
		
	}
	
	public void insertRowAfter(final int indexAtWhichRowInsertion)
	{
		Integer serialCounter = 1;
		
		for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableDataVO().size(); iCount++)
		{
			if(iCount == indexAtWhichRowInsertion)
			{
				ObjectHolder.getInstance().getTableDataVO().get(iCount).getSerialNumber().setValue((serialCounter++).toString());

				final ComboBoxVO comboBoxVO = new ComboBoxVO();
				
				final TableDataVO tableDataVO = new TableDataVO();
				tableDataVO.setImageView(null);
				tableDataVO.setSerialNumber(new SimpleStringProperty((serialCounter++).toString()));
				tableDataVO.setCommand(new SimpleObjectProperty<ComboBoxVO>(comboBoxVO));	
				ObjectHolder.getInstance().getTableDataVO().add(tableDataVO);
				iCount = iCount + 1;
			}
			else
			{
				ObjectHolder.getInstance().getTableDataVO().get(iCount).getSerialNumber().setValue((serialCounter++).toString());
			}
		}
	}

	public void insertRowBefore(final int indexAtWhichRowInsertion)
	{
		Integer serialCounter = 1;
		
		for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableDataVO().size(); iCount++)
		{
			if(iCount == indexAtWhichRowInsertion)
			{
				final ComboBoxVO comboBoxVO = new ComboBoxVO();
				
				final TableDataVO tableDataVO = new TableDataVO();
				tableDataVO.setImageView(null);
				tableDataVO.setSerialNumber(new SimpleStringProperty((serialCounter++).toString()));
				tableDataVO.setCommand(new SimpleObjectProperty<ComboBoxVO>(comboBoxVO));	
				ObjectHolder.getInstance().getTableDataVO().add(0, tableDataVO);

				ObjectHolder.getInstance().getTableDataVO().get(iCount).getSerialNumber().setValue((serialCounter++).toString());

				iCount = iCount + 1;
			}
			else
			{
				ObjectHolder.getInstance().getTableDataVO().get(iCount).getSerialNumber().setValue((serialCounter++).toString());
			}
		}
	}
	
	public void deleteSelectedRow(final int indexAtWhichRowInsertion)
	{
		Integer serialCounter = 1;
		
		for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableDataVO().size(); iCount++)
		{
			if(iCount == indexAtWhichRowInsertion)
			{
				ObjectHolder.getInstance().getTableDataVO().remove(iCount);
				if(iCount < ObjectHolder.getInstance().getTableDataVO().size())
				{
					ObjectHolder.getInstance().getTableDataVO().get(iCount).getSerialNumber().setValue((serialCounter++).toString());
				}
			}
			else
			{
				ObjectHolder.getInstance().getTableDataVO().get(iCount).getSerialNumber().setValue((serialCounter++).toString());
			}
		}
	}	
}