package com.saahas.robooperator.util;

import java.io.InputStream;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JAXBInstance 
{
	private static Marshaller marshaller = null;
	private static JAXBContext jaxbContext = null;
	private static JAXBInstance jaxbInstance = null;
	private static Unmarshaller unmarshaller = null;
	
	private JAXBInstance()
	{

	}
	
	public static JAXBInstance getJaxbInstance()
	{
		if(jaxbInstance == null)
		{
			synchronized (JAXBInstance.class) 
			{
				if(jaxbInstance == null)
				{
					jaxbInstance = new JAXBInstance();
				}
			}
		}
		return jaxbInstance;
	}
	

	public Object unMarshallDocument(final InputStream inputStream) throws Throwable
	{
		if(inputStream == null)
		{
			return null;
		}
		
		initializeUnmarshaller();
		
		return unmarshaller.unmarshal(inputStream);  
	}
	
	public void marshallDocument(final Object marshallObject, final Writer writer) throws Throwable
	{
		if(marshallObject == null)
		{
			return;
		}
		
		initializeMarshaller();
		
		marshaller.marshal(marshallObject, writer);
	}
	
	private void initializeJaxbContext() throws Throwable
	{
		if(jaxbContext == null)
		{
			synchronized (JAXBInstance.class) 
			{
				if(jaxbContext == null)
				{
					jaxbContext = JAXBContext.newInstance("com.saahas.robooperator.schema"); 
				}
			}
		}
	}

	private void initializeUnmarshaller() throws Throwable
	{
		initializeJaxbContext();
		
		if(unmarshaller == null)
		{
			synchronized (JAXBInstance.class) 
			{
				if(unmarshaller == null)
				{
					unmarshaller = jaxbContext.createUnmarshaller();
				}
			}
		}
	}

	private void initializeMarshaller() throws Throwable
	{
		initializeJaxbContext();
		
		if(marshaller == null)
		{
			synchronized (JAXBInstance.class) 
			{
				if(marshaller == null)
				{
					marshaller = jaxbContext.createMarshaller();
				}
			}
		}
		
	}
}
