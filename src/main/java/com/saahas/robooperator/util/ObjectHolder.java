package com.saahas.robooperator.util;

import java.util.List;
import java.util.Properties;

import com.saahas.robooperator.schema.RoboOperatorView;
import com.saahas.robooperator.vo.TableDataVO;

import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

public class ObjectHolder
{
	private Stage stage = null;
	private Scene scene = null;
	
	private Properties properties = null;
	private List<TableDataVO> tableDataVO = null;
	private TableView<TableDataVO> tableView = null;
	private RoboOperatorView roboOperatorView = null;

	private static ObjectHolder objectHolder = null; 

	public TableView<TableDataVO> getTableView()
	{
		return tableView;
	}

	public void setTableView(TableView<TableDataVO> tableView)
	{
		this.tableView = tableView;
	}

	private ObjectHolder()
	{
		
	}
	
	public static ObjectHolder getInstance()
	{
		if(objectHolder == null)
		{
			synchronized (ObjectHolder.class)
			{
				if(objectHolder == null)
				{
					objectHolder = new ObjectHolder();
				}
			}
		}
		
		return objectHolder;
	}
	
	public Stage getStage()
	{
		return stage;
	}

	public void setStage(Stage stage)
	{
		this.stage = stage;
	}

	public Scene getScene()
	{
		return scene;
	}

	public void setScene(Scene scene)
	{
		this.scene = scene;
	}

	public Properties getProperties()
	{
		return properties;
	}

	public void setProperties(Properties properties)
	{
		this.properties = properties;
	}

	public RoboOperatorView getRoboOperatorView()
	{
		return roboOperatorView;
	}

	public void setRoboOperatorView(RoboOperatorView roboOperatorView)
	{
		this.roboOperatorView = roboOperatorView;
	}

	public List<TableDataVO> getTableDataVO()
	{
		return tableDataVO;
	}

	public void setTableDataVO(List<TableDataVO> tableDataVO)
	{
		this.tableDataVO = tableDataVO;
	}
}
