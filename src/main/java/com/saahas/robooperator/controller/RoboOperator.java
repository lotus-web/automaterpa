package com.saahas.robooperator.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;

import com.saahas.robooperator.schema.RoboOperatorView;
import com.saahas.robooperator.util.JAXBInstance;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;
import com.saahas.robooperator.vo.ComboBoxVO;
import com.saahas.robooperator.vo.TableDataVO;

import javafx.application.Application;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class RoboOperator extends Application
{
	public static void main(String... commandLineArguments)
	{
		launch(commandLineArguments);
	}
	
	@Override
	public void start(final Stage roboOperatorStage) throws Exception
	{
		localStart(roboOperatorStage);
	}
	
	private void localStart(final Stage stage)
	{
		try
		{
			final Properties systemLocaleProperties = new RoboOperatorUtility()
					.getResourceBundleAsProperty(Locale.getDefault());
			if (
				systemLocaleProperties == null || systemLocaleProperties.isEmpty()
			)
			{
				System.exit(1);
			}
			ObjectHolder.getInstance().setProperties(systemLocaleProperties);
			
			final RoboOperatorView roboOperatorView = generateRoboOperatorView(systemLocaleProperties);
			if (
				roboOperatorView == null
			)
			{
				System.err.println(systemLocaleProperties.get("INVALID_CONFIG_FILE_CONTENT"));
				System.exit(1);
				
			}
			ObjectHolder.getInstance().setRoboOperatorView(roboOperatorView);
			
			final String language = identifyLanguage(roboOperatorView, systemLocaleProperties);
			if (
				language == null
			)
			{
				System.err.println(systemLocaleProperties.get("LANGUAGE_ELEMENT_MISSING"));
				System.exit(1);
			}
			
			final Image image = new RoboOperatorUtility().getImage("");
			if (
				image != null
			)
			{
				stage.getIcons().add(image);
			}
			
			ObjectHolder.getInstance().setStage(stage);
			ObjectHolder.getInstance().setTableView(new TableView<TableDataVO>());
			ObjectHolder.getInstance().setTableDataVO(new ArrayList<TableDataVO>());
			
			stage.setTitle("Robo Operator");
			stage.setResizable(false);
			stage.setWidth(600);
			stage.setHeight(600);
			
			final ComboBoxVO comboBoxVO = new ComboBoxVO();
			
			final TableDataVO tableDataVO = new TableDataVO();
			tableDataVO.setSerialNumber(new SimpleStringProperty("1"));
			tableDataVO.setCommand(new SimpleObjectProperty<ComboBoxVO>(comboBoxVO));
			tableDataVO.setImageView(null);
			tableDataVO.setKeyBoardEntry(null);
			
			ObjectHolder.getInstance().getTableDataVO().add(tableDataVO);
			
			new RoboOperatorScene().renderRoboOperatorScene();
			stage.show();
		}
		catch (Throwable eGenThrowable)
		{
			eGenThrowable.printStackTrace();
			System.err.println(eGenThrowable);
			System.exit(1);
		}
	}
	
	private RoboOperatorView generateRoboOperatorView(final Properties properties) throws Throwable
	{
		InputStream inputStream = null;
		
		try
		{
			inputStream = new RoboOperatorUtility().createAppConfigStream();
			if (
				inputStream == null
			)
			{
				System.err.println(properties.get("CONFIG_FILE_NOT_EXISTS"));
				return null;
			}
			
			return (RoboOperatorView) JAXBInstance.getJaxbInstance().unMarshallDocument(inputStream);
		}
		finally
		{
			new RoboOperatorUtility().cloaseAppConfigStream(inputStream);
		}
		
	}
	
	private String identifyLanguage(final RoboOperatorView roboOperatorView, final Properties properties)
			throws Throwable
	{
		
		new RoboOperatorUtility().validateLanguageElement();
		
		for (int iCount = 0; iCount < roboOperatorView.getLanguageList().getLanguage().size(); iCount++)
		{
			if (
				roboOperatorView.getLanguageList().getLanguage().get(iCount).getDefault() == null
						|| roboOperatorView.getLanguageList().getLanguage().get(iCount).getDefault().trim().equals("")
			)
			{
				continue;
			}
			
			if (
				roboOperatorView.getLanguageList().getLanguage().get(iCount).getDefault().equalsIgnoreCase("true")
			)
			{
				return roboOperatorView.getLanguageList().getLanguage().get(iCount).getName();
			}
		}
		
		return null;
	}
	
}
