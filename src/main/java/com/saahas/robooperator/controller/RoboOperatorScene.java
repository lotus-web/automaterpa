package com.saahas.robooperator.controller;

import com.saahas.robooperator.rendering.EmptyPane;
import com.saahas.robooperator.rendering.MenuBarC;
import com.saahas.robooperator.rendering.RoboOperatorTab;
import com.saahas.robooperator.rendering.RoboOperatorTableView;
import com.saahas.robooperator.rendering.TableAction;
import com.saahas.robooperator.util.ObjectHolder;

import javafx.scene.Scene;
import javafx.scene.layout.GridPane;

public class RoboOperatorScene
{

	public RoboOperatorScene()
	{

	}

	public void renderRoboOperatorScene() throws Throwable
	{
		int rowIndex = 0;
		int columnIndex = 0;
		
		final GridPane sceneGridPaen = new GridPane();

		sceneGridPaen.add(new MenuBarC().render(), columnIndex, rowIndex++);
		sceneGridPaen.add(new EmptyPane().render(6), columnIndex, rowIndex++);
		sceneGridPaen.add(new RoboOperatorTab().render(), columnIndex, rowIndex++);
		sceneGridPaen.add(new EmptyPane().render(6), columnIndex, rowIndex++);
		sceneGridPaen.add(new TableAction().render(), columnIndex, rowIndex++);
		sceneGridPaen.add(new EmptyPane().render(6), columnIndex, rowIndex++);
		sceneGridPaen.add(new RoboOperatorTableView().render(), columnIndex, rowIndex++);

		for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableDataVO().size(); iCount++)
		{
			ObjectHolder.getInstance().getTableView().getItems().add(ObjectHolder.getInstance().getTableDataVO().get(iCount));
		}
		
		final Scene scene = new Scene(sceneGridPaen);
		scene.getStylesheets().add(getClass().getResource("/styles/roboOperatorStyles.css").toExternalForm());

		ObjectHolder.getInstance().getStage().setScene(scene);
		
	}
}
