package com.saahas.robooperator.tablecell;

import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;
import com.saahas.robooperator.vo.ComboBoxVO;
import com.saahas.robooperator.vo.TableDataVO;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.beans.value.ChangeListener;

public class ComboBoxCell<S, T> extends TableCell<TableDataVO, ComboBoxVO>
{
    private final ComboBox<String> comboBox ;

    public ComboBoxCell() {
    	
    	final ObservableList<String> observableList = FXCollections.observableArrayList();
    	generateCombBoxValue(observableList);

        this.comboBox = new ComboBox<>(observableList);
        this.comboBox.prefWidthProperty().bind((ObjectHolder.getInstance().getStage().widthProperty().multiply(0.2)));

        comboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> obs, String oldValue, String newValue) {
            	ObservableValue<ComboBoxVO> property = getTableColumn().getCellObservableValue(getIndex());
            	property.getValue().setValue(newValue);
            }
        });
        
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

    }

    @Override
    public void updateItem(ComboBoxVO item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
        } else {
            comboBox.setValue(item.getValue());
            setGraphic(comboBox);
        }
    }
    
    private void generateCombBoxValue(final ObservableList<String> observableList)
    {
    	try
    	{
	    	final RoboOperatorUtility roboOperatorUtility = new RoboOperatorUtility();
	   		observableList.add(roboOperatorUtility.getProperty("OPEN"));
	   		observableList.add(roboOperatorUtility.getProperty("CAPTURE"));
	   		observableList.add(roboOperatorUtility.getProperty("CLICK"));
	   		
    	}
    	catch(Throwable eGenThrowable)
    	{
    		
    	}
    }
}