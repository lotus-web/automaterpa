package com.saahas.robooperator.tablecell;

import com.saahas.robooperator.vo.TableDataVO;
import javafx.scene.control.TableCell;


public class SerialNumberCell<S, T> extends TableCell<TableDataVO, String>
{
	public SerialNumberCell()
	{

	}

	@Override
	public void updateItem(String item, boolean empty)
	{
		super.updateItem(item, empty);
		setText(item);
		setGraphic(null);
	}
}
