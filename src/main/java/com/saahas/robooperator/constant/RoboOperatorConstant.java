package com.saahas.robooperator.constant;

import java.util.HashMap;
import java.util.Map;

import com.saahas.robooperator.vo.LocaleVO;

public class RoboOperatorConstant
{
	public static final String APP_CONFIG_FILE = "APP_CONFIG_FILE";
	public static final Map<String, LocaleVO> LOCALE_VO = new HashMap<String, LocaleVO>()
	{
		private static final long serialVersionUID = 1L;
		{
			put("English", new LocaleVO("en", "US"));
			put("Japanese", new LocaleVO("ja", "JP"));
		}
	};
	
}
