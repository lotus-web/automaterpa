//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.09.01 at 01:14:57 PM IST 
//


package com.saahas.robooperator.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="menuListC" type="{}menuListC"/>
 *         &lt;element name="languageList" type="{}languageList"/>
 *         &lt;element name="tablist" type="{}tablist"/>
 *         &lt;element name="rowOperationList" type="{}rowOperationList"/>
 *         &lt;element name="commandList" type="{}commandList"/>
 *         &lt;element name="logging" type="{}logging"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "menuListC",
    "languageList",
    "tablist",
    "rowOperationList",
    "commandList",
    "logging"
})
@XmlRootElement(name = "roboOperatorView")
public class RoboOperatorView {

    @XmlElement(required = true)
    protected MenuListC menuListC;
    @XmlElement(required = true)
    protected LanguageList languageList;
    @XmlElement(required = true)
    protected Tablist tablist;
    @XmlElement(required = true)
    protected RowOperationList rowOperationList;
    @XmlElement(required = true)
    protected CommandList commandList;
    @XmlElement(required = true)
    protected Logging logging;

    /**
     * Gets the value of the menuListC property.
     * 
     * @return
     *     possible object is
     *     {@link MenuListC }
     *     
     */
    public MenuListC getMenuListC() {
        return menuListC;
    }

    /**
     * Sets the value of the menuListC property.
     * 
     * @param value
     *     allowed object is
     *     {@link MenuListC }
     *     
     */
    public void setMenuListC(MenuListC value) {
        this.menuListC = value;
    }

    /**
     * Gets the value of the languageList property.
     * 
     * @return
     *     possible object is
     *     {@link LanguageList }
     *     
     */
    public LanguageList getLanguageList() {
        return languageList;
    }

    /**
     * Sets the value of the languageList property.
     * 
     * @param value
     *     allowed object is
     *     {@link LanguageList }
     *     
     */
    public void setLanguageList(LanguageList value) {
        this.languageList = value;
    }

    /**
     * Gets the value of the tablist property.
     * 
     * @return
     *     possible object is
     *     {@link Tablist }
     *     
     */
    public Tablist getTablist() {
        return tablist;
    }

    /**
     * Sets the value of the tablist property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tablist }
     *     
     */
    public void setTablist(Tablist value) {
        this.tablist = value;
    }

    /**
     * Gets the value of the rowOperationList property.
     * 
     * @return
     *     possible object is
     *     {@link RowOperationList }
     *     
     */
    public RowOperationList getRowOperationList() {
        return rowOperationList;
    }

    /**
     * Sets the value of the rowOperationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link RowOperationList }
     *     
     */
    public void setRowOperationList(RowOperationList value) {
        this.rowOperationList = value;
    }

    /**
     * Gets the value of the commandList property.
     * 
     * @return
     *     possible object is
     *     {@link CommandList }
     *     
     */
    public CommandList getCommandList() {
        return commandList;
    }

    /**
     * Sets the value of the commandList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommandList }
     *     
     */
    public void setCommandList(CommandList value) {
        this.commandList = value;
    }

    /**
     * Gets the value of the logging property.
     * 
     * @return
     *     possible object is
     *     {@link Logging }
     *     
     */
    public Logging getLogging() {
        return logging;
    }

    /**
     * Sets the value of the logging property.
     * 
     * @param value
     *     allowed object is
     *     {@link Logging }
     *     
     */
    public void setLogging(Logging value) {
        this.logging = value;
    }

}
