package com.saahas.robooperator.eventhandler;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

import org.sikuli.script.App;
import org.sikuli.script.Image;
import org.sikuli.script.Key;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.saahas.robooperator.util.ObjectHolder;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class RunCommand implements EventHandler<ActionEvent>
{
	@Override
	public void handle(final ActionEvent event)
	{
		try
		{

			ObjectHolder.getInstance().getStage().toBack();

			for(int iCount = 0; iCount < ObjectHolder.getInstance().getTableDataVO().size(); iCount++)
			{
				App.open(System.getProperty("APP_PATH"));
				final Screen screen = Screen.getPrimaryScreen();
				final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(ObjectHolder.getInstance().getTableDataVO().get(iCount).getByteArrayOutputStream().toByteArray()));
				final Pattern pattern = new Pattern(new Image(bufferedImage));
				final Match match = screen.exists(pattern);
				if(match != null)
				{
					screen.type(match, ObjectHolder.getInstance().getTableDataVO().get(iCount).getKeyBoardEntry().getValue() + Key.ENTER);
				}
				else
				{
					System.out.println("Not Matching");
				}

			}
			
			ObjectHolder.getInstance().getStage().toFront();

		}
		catch(Throwable eGenThrowable)
		{
			eGenThrowable.printStackTrace();
		}
	}
}
