package com.saahas.robooperator.eventhandler;

import com.saahas.robooperator.controller.RoboOperatorScene;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DeleteRow implements EventHandler<ActionEvent>
{
	@Override
	public void handle(final ActionEvent event)
	{
		
		try
		{
			final int iSelectedRowCount = ObjectHolder.getInstance().getTableView().getSelectionModel().getSelectedIndex();
			if(iSelectedRowCount == - 1)
			{
				return;
			}
			else
			{
				new RoboOperatorUtility().resetTableView();
				new RoboOperatorUtility().deleteSelectedRow(iSelectedRowCount);
			}
			
			new RoboOperatorScene().renderRoboOperatorScene();
		}
		catch(Throwable eGenThrowable)
		{
			
		}		
	}

}
