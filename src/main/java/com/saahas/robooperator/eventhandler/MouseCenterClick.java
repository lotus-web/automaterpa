package com.saahas.robooperator.eventhandler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.sikuli.script.ScreenImage;

import com.saahas.robooperator.controller.RoboOperatorScene;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class MouseCenterClick implements EventHandler<ActionEvent>
{
	@Override
	public void handle(final ActionEvent event)
	{
		try
		{
			int iSelectedRowCount = ObjectHolder.getInstance().getTableView().getSelectionModel().getSelectedIndex();
			if (iSelectedRowCount == -1)
			{
				iSelectedRowCount = 0;
			}
			ObjectHolder.getInstance().getStage().toBack();

			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			final Screen screen = Screen.getPrimaryScreen();
			final Region region = screen.selectRegion("Select Area to capture as Image");
			final ScreenImage capturedRegion = screen.capture(region.x, region.y, region.w, region.h);
			ImageIO.write(capturedRegion.getImage(), "PNG", byteArrayOutputStream);
			
			new RoboOperatorUtility().resetTableView();

			final ImageView imageView = new ImageView();
			final Image image = new Image(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
			imageView.setImage(image);
			imageView.setFitHeight(50);
			imageView.setFitWidth(ObjectHolder.getInstance().getTableView().prefWidthProperty().multiply(0.3).doubleValue());
			ObjectHolder.getInstance().getTableDataVO().get(iSelectedRowCount).setImageView(imageView);

			ObjectHolder.getInstance().getStage().toFront();
			new RoboOperatorScene().renderRoboOperatorScene();
		}
		catch(Throwable eGenThrowable)
		{

		}
	}

}
