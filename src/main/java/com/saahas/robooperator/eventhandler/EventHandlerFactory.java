package com.saahas.robooperator.eventhandler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class EventHandlerFactory
{
	public EventHandler<ActionEvent> getEventHandler(final String actionName)
	{
		if(actionName.equalsIgnoreCase("InsertBefore"))
		{
			return new InsertRowBefore();
		}
		
		if(actionName.equalsIgnoreCase("InsertAfter"))
		{
			return new InsertRowAfter();
		}

		if(actionName.equalsIgnoreCase("DeleteRow"))
		{
			return new DeleteRow();
		}
		
		if(actionName.equalsIgnoreCase("RightClick"))
		{
			return new MouseRightClick();
		}		

		if(actionName.equalsIgnoreCase("LeftClick"))
		{
			return new MouseLeftClick();
		}		

		if(actionName.equalsIgnoreCase("CenterClick"))
		{
			return new MouseCenterClick();
		}	
		
		if(actionName.equalsIgnoreCase("Run"))
		{
			return new RunCommand();
		}			
		return null;
	}
}
