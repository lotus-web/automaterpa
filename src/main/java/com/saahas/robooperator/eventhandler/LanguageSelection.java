package com.saahas.robooperator.eventhandler;

import java.util.List;
import java.util.Locale;
import java.util.Properties;

import com.saahas.robooperator.constant.RoboOperatorConstant;
import com.saahas.robooperator.controller.RoboOperatorScene;
import com.saahas.robooperator.schema.Language;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;
import com.saahas.robooperator.vo.LocaleVO;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class LanguageSelection implements ChangeListener<String>
{

	public LanguageSelection()
	{

	}

	public void changed(ObservableValue<? extends String> observable, String oldlanguage, String newlanguage)
	{
		if(newlanguage == null || newlanguage.trim().equals(""))
		{
			return;
		}
		
		final LocaleVO localeVO = RoboOperatorConstant.LOCALE_VO.get(newlanguage);
		if(localeVO == null)
		{
			return;
		}		
		
		
		try
		{
			final Properties selectedLocaleProperties = new RoboOperatorUtility().getResourceBundleAsProperty(new Locale(localeVO.getLanguage(), localeVO.getCountry()));
			if(selectedLocaleProperties == null || selectedLocaleProperties.isEmpty())
			{
				return;
			}
			
			final List<Language> languageList = ObjectHolder.getInstance().getRoboOperatorView().getLanguageList().getLanguage();
			
			for(int iCount = 0; iCount < languageList.size(); iCount++)
			{
				if(newlanguage.equals(languageList.get(iCount).getName()))
				{
					languageList.get(iCount).setDefault("true");
				}
				else
				{
					languageList.get(iCount).setDefault("false");
				}
			}
			
			new RoboOperatorUtility().resetTableView();
			
			ObjectHolder.getInstance().setProperties(selectedLocaleProperties);

			new RoboOperatorScene().renderRoboOperatorScene();
		}
		catch(Throwable eGenThrowable)
		{
			eGenThrowable.printStackTrace();
		}
	}

}
