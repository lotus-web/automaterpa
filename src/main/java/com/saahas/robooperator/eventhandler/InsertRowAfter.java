package com.saahas.robooperator.eventhandler;

import com.saahas.robooperator.controller.RoboOperatorScene;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class InsertRowAfter implements EventHandler<ActionEvent>
{
	@Override
	public void handle(final ActionEvent event)
	{
		try
		{
			new RoboOperatorUtility().resetTableView();
			
			final int iSelectedRowCount = ObjectHolder.getInstance().getTableView().getSelectionModel().getSelectedIndex();
			if(iSelectedRowCount == - 1)
			{
				new RoboOperatorUtility().insertRowAfter(0);
			}
			else
			{
				new RoboOperatorUtility().insertRowAfter(iSelectedRowCount);
			}
			
			new RoboOperatorScene().renderRoboOperatorScene();
		}
		catch(Throwable eGenThrowable)
		{
			
		}

	}

}
