package com.saahas.robooperator.vo;

public class LocaleVO
{
	private String country = null;
	private String language = null;
	
	public LocaleVO(final String language, final String country)
	{
		super();
		this.country = country;
		this.language = language;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public String getLanguage()
	{
		return language;
	}
	public void setLanguage(String language)
	{
		this.language = language;
	}
	
	
}
