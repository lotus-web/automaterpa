package com.saahas.robooperator.vo;

import java.io.ByteArrayOutputStream;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.ImageView;

public class TableDataVO
{
	private SimpleStringProperty keyBoardEntry;
    private ImageView imageView;
    private SimpleStringProperty serialNumber;
    private SimpleObjectProperty<ComboBoxVO> command;
    private ByteArrayOutputStream byteArrayOutputStream;

    
	
	public ByteArrayOutputStream getByteArrayOutputStream()
	{
		return byteArrayOutputStream;
	}
	public void setByteArrayOutputStream(ByteArrayOutputStream byteArrayOutputStream)
	{
		this.byteArrayOutputStream = byteArrayOutputStream;
	}
	public SimpleStringProperty getKeyBoardEntry()
	{
		return keyBoardEntry;
	}
	public void setKeyBoardEntry(SimpleStringProperty keyBoardEntry)
	{
		this.keyBoardEntry = keyBoardEntry;
	}
	public SimpleStringProperty getSerialNumber()
	{
		return serialNumber;
	}
	public void setSerialNumber(SimpleStringProperty serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public SimpleObjectProperty<ComboBoxVO> getCommand()
	{
		return command;
	}
	public void setCommand(SimpleObjectProperty<ComboBoxVO> command)
	{
		this.command = command;
	}
	public ImageView getImageView()
	{
		return imageView;
	}
	public void setImageView(ImageView imageView)
	{
		this.imageView = imageView;
	}
}
