package com.saahas.robooperator.vo;

import javafx.beans.property.SimpleStringProperty;

public class ComboBoxVO
{
    private final SimpleStringProperty comboxBoxValue;
    
    public ComboBoxVO() {
        this.comboxBoxValue = new SimpleStringProperty();
    }

    public String getValue() {
        return this.comboxBoxValue.get();
    }

    public void setValue(final String item) {
        this.comboxBoxValue.set(item);
    }
}
