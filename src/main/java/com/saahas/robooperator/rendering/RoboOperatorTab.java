package com.saahas.robooperator.rendering;

import java.util.ArrayList;
import java.util.List;

import com.saahas.robooperator.eventhandler.EventHandlerFactory;
import com.saahas.robooperator.schema.RoboOperatorView;
import com.saahas.robooperator.schema.TabC;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class RoboOperatorTab
{
	private RoboOperatorUtility roboOperatorUtility = null;
	
	public RoboOperatorTab()
	{
		this.roboOperatorUtility = new RoboOperatorUtility();
	}
	
	public Pane render()
	throws Throwable
	{
		final RoboOperatorView roboOperatorView = ObjectHolder.getInstance().getRoboOperatorView();
		
		final TabPane tabPane = new TabPane();
		tabPane.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tabPane.setBackground(new Background(new BackgroundFill(Color.rgb(148, 201, 234), CornerRadii.EMPTY, Insets.EMPTY)));
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabPane.tabMinWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty().divide(roboOperatorView.getTablist().getTabC().size()).subtract(25));

		for(int iCount = 0; iCount < roboOperatorView.getTablist().getTabC().size(); iCount++)
		{
            final Tab tab = new Tab();
            tab.setText(roboOperatorUtility.getProperty(roboOperatorView.getTablist().getTabC().get(iCount).getName()));
            tab.setContent(createTabItems(roboOperatorView.getTablist().getTabC().get(iCount)));
            tabPane.getTabs().add(tab);
		}

		
		final HBox tabPaneHBox = new HBox();
		tabPaneHBox.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tabPaneHBox.getChildren().add(tabPane);
		
		return tabPaneHBox;
		
	}
	
	public Pane createTabItems(final TabC tabC) throws Throwable
	{
		final GridPane tabeItemGridPane = new GridPane();
		tabeItemGridPane.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tabeItemGridPane.setBackground(new Background(new BackgroundFill(Color.rgb(64, 136, 182), CornerRadii.EMPTY, Insets.EMPTY)));
		tabeItemGridPane.setStyle("-fx-padding: 1;" + 
                "-fx-border-style: solid inside;" + 
                "-fx-border-width: 2;" +
                "-fx-border-insets: 1;" + 
                "-fx-border-radius: 1;" + 
                "-fx-border-color: #4088B6;");		
		final List<ColumnConstraints> columnConstraintList = new ArrayList<ColumnConstraints>();
		
		for(int iCount = 0; iCount < tabC.getTabItemListC().size(); iCount++)
		{
			final ColumnConstraints languaugeComboBoxConstraint = new ColumnConstraints();
			languaugeComboBoxConstraint.setPercentWidth(100 / tabC.getTabItemListC().size());	
			languaugeComboBoxConstraint.setHalignment(HPos.LEFT);
			columnConstraintList.add(languaugeComboBoxConstraint);
			tabeItemGridPane.getColumnConstraints().add(languaugeComboBoxConstraint);
		}
		
		for(int iCount = 0; iCount < tabC.getTabItemListC().size(); iCount++)
		{
			final Button button = new Button(roboOperatorUtility.getProperty(tabC.getTabItemListC().get(iCount).getName()));
			button.setGraphic(roboOperatorUtility.getImageView(tabC.getTabItemListC().get(iCount).getImagePath()));
			button.setPrefWidth(columnConstraintList.get(iCount).getPercentWidth() * 100);
			button.setOnAction(new EventHandlerFactory().getEventHandler(tabC.getTabItemListC().get(iCount).getName()));
			tabeItemGridPane.add(button, iCount, 0);
		}
		
		return tabeItemGridPane;
	}

}
