package com.saahas.robooperator.rendering;

import java.util.ArrayList;
import java.util.List;

import com.saahas.robooperator.eventhandler.EventHandlerFactory;
import com.saahas.robooperator.schema.RoboOperatorView;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class TableAction
{
	private RoboOperatorUtility roboOperatorUtility = null;
	
	public TableAction()
	{
		this.roboOperatorUtility = new RoboOperatorUtility();
	}
	
	public Pane render()
	throws Throwable
	{
		final GridPane tableActionGrid = new GridPane();
		final RoboOperatorView roboOperatorView = ObjectHolder.getInstance().getRoboOperatorView();
		
		final List<ColumnConstraints> columnConstraintList = new ArrayList<ColumnConstraints>();

		tableActionGrid.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tableActionGrid.setBackground(new Background(new BackgroundFill(Color.rgb(201, 211, 217), CornerRadii.EMPTY, Insets.EMPTY)));
		tableActionGrid.setStyle("-fx-padding: 1;" + 
                "-fx-border-style: solid inside;" + 
                "-fx-border-width: 2;" +
                "-fx-border-insets: 3;" + 
                "-fx-border-radius: 3;" + 
                "-fx-border-color: #C9D3D9;");		

		for(int iCount = 0; iCount < roboOperatorView.getRowOperationList().getRowOperation().size(); iCount++)
		{
			final ColumnConstraints languaugeComboBoxConstraint = new ColumnConstraints();
			languaugeComboBoxConstraint.setPercentWidth(100 / roboOperatorView.getRowOperationList().getRowOperation().size());	
			languaugeComboBoxConstraint.setHalignment(HPos.LEFT);
			columnConstraintList.add(languaugeComboBoxConstraint);
			tableActionGrid.getColumnConstraints().add(languaugeComboBoxConstraint);
		}
		
		for(int iCount = 0; iCount < roboOperatorView.getRowOperationList().getRowOperation().size(); iCount++)
		{
			final Button button = new Button(roboOperatorUtility.getProperty(roboOperatorView.getRowOperationList().getRowOperation().get(iCount).getName()));
			button.setGraphic(roboOperatorUtility.getImageView(roboOperatorView.getRowOperationList().getRowOperation().get(iCount).getImagePath()));
			button.setPrefWidth(columnConstraintList.get(iCount).getPercentWidth() * 100);
			tableActionGrid.add(button, iCount, 0);
			
			button.setOnAction(new EventHandlerFactory().getEventHandler(roboOperatorView.getRowOperationList().getRowOperation().get(iCount).getName()));
		}
		
		final HBox rowOperationHBox = new HBox();
		rowOperationHBox.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		rowOperationHBox.getChildren().add(tableActionGrid);
		
		return rowOperationHBox;

	}

}
