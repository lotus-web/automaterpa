package com.saahas.robooperator.rendering;

import com.saahas.robooperator.util.ObjectHolder;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;

public class EmptyPane
{
	public Pane render(final int rowConstranitSize)
	throws Throwable
	{
		final GridPane gridPane = new GridPane();
		gridPane.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		
		final RowConstraints rowConstraints = new RowConstraints(rowConstranitSize);
		gridPane.getRowConstraints().add(rowConstraints);

		final HBox tabPaneHBox = new HBox();
		tabPaneHBox.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tabPaneHBox.getChildren().add(gridPane);
		return tabPaneHBox;
		
	}
}
