package com.saahas.robooperator.rendering;

import java.util.List;

import com.saahas.robooperator.eventhandler.EventHandlerFactory;
import com.saahas.robooperator.eventhandler.LanguageSelection;
import com.saahas.robooperator.schema.MenuItemListC;
import com.saahas.robooperator.schema.RoboOperatorView;
import com.saahas.robooperator.schema.SubMenu;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class MenuBarC
{
	private RoboOperatorUtility roboOperatorUtility = null;

	public MenuBarC()
	{
		roboOperatorUtility = new RoboOperatorUtility();
	}
	
	public Pane render() throws Throwable
	{
		final GridPane menuBarGrid = new GridPane();
		menuBarGrid.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		
		final ColumnConstraints menuBarConstraint = new ColumnConstraints();
        menuBarConstraint.setPercentWidth(82);	
		menuBarGrid.getColumnConstraints().add(menuBarConstraint);
		
		final ColumnConstraints languaugeConstraint = new ColumnConstraints();
		languaugeConstraint.setPercentWidth(18);	
		languaugeConstraint.setHalignment(HPos.LEFT);
		menuBarGrid.getColumnConstraints().add(languaugeConstraint);

		final MenuBar menuBar = createMenuBar();
		menuBarGrid.add(menuBar, 0, 0);

		final ComboBox<String> comboBox = createLanguageComboBox();
		menuBarGrid.add(comboBox, 1, 0);

		menuBarGrid.setBackground(new Background(new BackgroundFill(Color.rgb(51, 104, 136), CornerRadii.EMPTY, Insets.EMPTY)));
		
		final HBox menuBarHBox = new HBox();
		menuBarHBox.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		menuBarHBox.getChildren().add(menuBarGrid);
		
		return menuBarHBox;
	}
	
	private ComboBox<String> createLanguageComboBox()
	throws Throwable
	{
		roboOperatorUtility.validateLanguageElement();
		
		final RoboOperatorView roboOperatorView = ObjectHolder.getInstance().getRoboOperatorView();

		final ComboBox<String> comboBox = new ComboBox<String>();
		for(int iCount = 0; iCount < roboOperatorView.getLanguageList().getLanguage().size(); iCount++)
		{
			comboBox.getItems().add(roboOperatorView.getLanguageList().getLanguage().get(iCount).getName());
			
			if(roboOperatorView.getLanguageList().getLanguage().get(iCount).getDefault() == null || roboOperatorView.getLanguageList().getLanguage().get(iCount).getDefault().trim().equals(""))
			{
				continue;
			}
			
			if(roboOperatorView.getLanguageList().getLanguage().get(iCount).getDefault().equals("true"))
			{
				comboBox.getSelectionModel().select(iCount);
			}
		}
		
		comboBox.valueProperty().addListener(new LanguageSelection());
		return comboBox;
	}

	private MenuBar createMenuBar()
	throws Throwable
	{
		roboOperatorUtility.validateMenuListElement();	

		final MenuBar menuBar = new MenuBar();
		final RoboOperatorView roboOperatorView = ObjectHolder.getInstance().getRoboOperatorView();

		for(int iCount = 0; iCount < roboOperatorView.getMenuListC().getMenuC().size(); iCount++)
		{
			final Menu menu = new Menu(roboOperatorUtility.getProperty(roboOperatorView.getMenuListC().getMenuC().get(iCount).getName()));
			menu.setGraphic(roboOperatorUtility.getImageView(roboOperatorView.getMenuListC().getMenuC().get(iCount).getImagePath()));
			
			createMenuItem(menu, roboOperatorView.getMenuListC().getMenuC().get(iCount).getMenuItemListC());
			
			menuBar.getMenus().add(menu);
		}
		
		return menuBar;
	}
	
	private void createMenuItem(final Menu menu, final List<MenuItemListC> menuItemsList)
	throws Throwable
	{
		if(menuItemsList == null || menuItemsList.size() == 0)
		{
			throw new Exception(roboOperatorUtility.getProperty("MENUITEMLIST_ELEMENT_MISSING"));
		}
		
		for(int iCount = 0; iCount < menuItemsList.size(); iCount++)
		{
			if(menuItemsList.get(iCount).getSubMenu() == null || menuItemsList.get(iCount).getSubMenu().size() == 0)
			{
				final MenuItem menuItem = new MenuItem(roboOperatorUtility.getProperty(menuItemsList.get(iCount).getName()));
				menuItem.setGraphic(roboOperatorUtility.getImageView(menuItemsList.get(iCount).getImagePath()));
				menuItem.setOnAction(new EventHandlerFactory().getEventHandler(menuItemsList.get(iCount).getName()));
				menu.getItems().add(menuItem);
			}
			else
			{
				final Menu subMenu = new Menu(roboOperatorUtility.getProperty(menuItemsList.get(iCount).getName()));
				createSubMenu(subMenu, menuItemsList.get(iCount).getSubMenu());
				menu.getItems().add(subMenu);
			}
		}
	}

	private void createSubMenu(final Menu menu, final List<SubMenu> subMenuList)
	throws Throwable
	{
		for(int iCount = 0; iCount < subMenuList.size(); iCount++)
		{
			if(subMenuList.get(iCount).getSubMenu().size() == 0)
			{
				final MenuItem menuItem = new MenuItem(roboOperatorUtility.getProperty(subMenuList.get(iCount).getName()));
				menuItem.setGraphic(roboOperatorUtility.getImageView(subMenuList.get(iCount).getImagePath()));
				menu.getItems().add(menuItem);
			}
			else
			{
				final Menu subMenu = new Menu(subMenuList.get(iCount).getName());
				createSubMenu(subMenu, subMenuList.get(iCount).getSubMenu());
				menu.getItems().add(subMenu);
			}
		}
	}
}
