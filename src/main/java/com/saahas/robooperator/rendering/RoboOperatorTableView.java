package com.saahas.robooperator.rendering;

import com.saahas.robooperator.tablecell.ComboBoxCell;
import com.saahas.robooperator.tablecell.KeyBoardEntryCell;
import com.saahas.robooperator.tablecell.SerialNumberCell;
import com.saahas.robooperator.util.ObjectHolder;
import com.saahas.robooperator.util.RoboOperatorUtility;
import com.saahas.robooperator.vo.ComboBoxVO;
import com.saahas.robooperator.vo.TableDataVO;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.Callback;;

public class RoboOperatorTableView
{
	private RoboOperatorUtility roboOperatorUtility = null;
	
	public RoboOperatorTableView()
	{
		this.roboOperatorUtility = new RoboOperatorUtility();
	}
	
	public Pane render()
	throws Throwable
	{
		final TableView<TableDataVO> tableView = ObjectHolder.getInstance().getTableView();
		createSerailNumber();
		createComboBox();
		createImageView();
		createEditableTextField();
		tableView.setEditable(true);
		
		tableView.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tableView.setFixedCellSize(50);
		
		if(ObjectHolder.getInstance().getTableDataVO().size() == 0)
		{
			tableView.prefHeightProperty().bind(tableView.fixedCellSizeProperty().multiply(2).add(1.01));
		}
		else
		{
			tableView.prefHeightProperty().bind(tableView.fixedCellSizeProperty().multiply(ObjectHolder.getInstance().getTableDataVO().size() + 0.54));
		}
	    
		final HBox tablePaneHBox = new HBox();
		tablePaneHBox.prefWidthProperty().bind(ObjectHolder.getInstance().getStage().widthProperty());
		tablePaneHBox.getChildren().add(tableView);

		return tablePaneHBox;
	}

	private void createSerailNumber()
	throws Throwable
	{
        
        final Callback<TableColumn<TableDataVO, String>, TableCell<TableDataVO, String>> serialCellFactory
        = (TableColumn<TableDataVO, String> param) -> new SerialNumberCell<TableDataVO, String>();
		
        final TableColumn<TableDataVO, String> serailNumberColumn = new TableColumn<TableDataVO, String>(roboOperatorUtility.getProperty("SI"));
        serailNumberColumn.setMinWidth(100);
        serailNumberColumn.setCellValueFactory(cellData -> cellData.getValue().getSerialNumber());
        serailNumberColumn.setCellFactory(serialCellFactory);
        serailNumberColumn.prefWidthProperty().bind(ObjectHolder.getInstance().getTableView().widthProperty().multiply(0.01));
        serailNumberColumn.setStyle( "-fx-alignment: CENTER;");

        ObjectHolder.getInstance().getTableView().getColumns().add(serailNumberColumn);
 	}
	
	private void createComboBox() 
	throws Throwable
	{
        final Callback<TableColumn<TableDataVO, ComboBoxVO>, TableCell<TableDataVO, ComboBoxVO>> commandCellFactory
        = (TableColumn<TableDataVO, ComboBoxVO> param) -> new ComboBoxCell<TableDataVO, ComboBoxVO>();
        
        final TableColumn<TableDataVO, ComboBoxVO> commandColumn = new TableColumn<TableDataVO, ComboBoxVO>(roboOperatorUtility.getProperty("COMMAND"));
        commandColumn.setCellValueFactory(cellData -> 
        {
        	return cellData.getValue().getCommand();
        });
        commandColumn.setCellFactory(commandCellFactory);
        commandColumn.prefWidthProperty().bind(ObjectHolder.getInstance().getTableView().widthProperty().multiply(0.2));
        commandColumn.setStyle( "-fx-alignment: CENTER;");
        ObjectHolder.getInstance().getTableView().getColumns().add(commandColumn);
	}
	
	
	private void createImageView() 
	throws Throwable

	{
        final TableColumn<TableDataVO, ImageView> actionColumn = new TableColumn<TableDataVO, ImageView>(roboOperatorUtility.getProperty("ACTION"));
        actionColumn.setMinWidth(100);
        actionColumn.setCellValueFactory(new PropertyValueFactory<TableDataVO, ImageView>("imageView"));
        actionColumn.prefWidthProperty().bind(ObjectHolder.getInstance().getTableView().widthProperty().multiply(0.3));
        actionColumn.setStyle( "-fx-alignment: CENTER-LEFT;");

        ObjectHolder.getInstance().getTableView().getColumns().add(actionColumn);
	}

	private void createEditableTextField()
	throws Throwable
	{
        
        final Callback<TableColumn<TableDataVO, String>, TableCell<TableDataVO, String>> serialCellFactory
        = (TableColumn<TableDataVO, String> param) -> new KeyBoardEntryCell<TableDataVO, String>();
		
        final TableColumn<TableDataVO, String> keyBoardColumn = new TableColumn<TableDataVO, String>(roboOperatorUtility.getProperty("KeyBoardEntry"));
        keyBoardColumn.setMinWidth(100);
        keyBoardColumn.setCellValueFactory(cellData -> cellData.getValue().getKeyBoardEntry());
        keyBoardColumn.setCellFactory(serialCellFactory);
        keyBoardColumn.prefWidthProperty().bind(ObjectHolder.getInstance().getTableView().widthProperty().multiply(0.3));
        keyBoardColumn.setStyle( "-fx-alignment: CENTER;");
        keyBoardColumn.setEditable(true);
        keyBoardColumn.setOnEditCommit(
                (TableColumn.CellEditEvent<TableDataVO, String> t) -> {
                    ((TableDataVO) t.getTableView().getItems()
                    .get(t.getTablePosition().getRow()))
                    .setKeyBoardEntry(new SimpleStringProperty(t.getNewValue()));
                    

                });

        ObjectHolder.getInstance().getTableView().getColumns().add(keyBoardColumn);
 	}
}
